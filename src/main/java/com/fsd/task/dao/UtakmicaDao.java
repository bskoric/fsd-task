package com.fsd.task.dao;

import com.fsd.task.model.UtakmicaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtakmicaDao extends JpaRepository<UtakmicaModel, Integer> {
}
