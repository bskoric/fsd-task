package com.fsd.task.dao;

import com.fsd.task.model.IgracModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IgracDao extends JpaRepository<IgracModel, Integer> {
}
