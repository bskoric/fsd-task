package com.fsd.task.dao;

import com.fsd.task.model.KlubModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KlubDao extends JpaRepository<KlubModel, Integer> {
}
