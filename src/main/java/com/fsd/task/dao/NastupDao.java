package com.fsd.task.dao;

import com.fsd.task.model.NastupModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface NastupDao extends JpaRepository<NastupModel, Integer> {

    @Query(value = "SELECT n FROM NastupModel n WHERE n.igrac.id = :igracId")
    NastupModel findByIgracId(@Param("igracId") int igracId);
}
