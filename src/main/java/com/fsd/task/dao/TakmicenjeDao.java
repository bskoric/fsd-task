package com.fsd.task.dao;

import com.fsd.task.model.TakmicenjeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TakmicenjeDao extends JpaRepository<TakmicenjeModel, Integer> {
}
