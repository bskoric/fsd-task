package com.fsd.task.dao;

import com.fsd.task.model.NastupModel;

public interface NastupDaoCustom {

    NastupModel findByIgracId(int igracId);
}
