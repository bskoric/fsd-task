package com.fsd.task.dao;

import com.fsd.task.model.MestoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MestoDao extends JpaRepository<MestoModel, Integer> {
}
