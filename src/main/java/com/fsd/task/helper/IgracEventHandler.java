package com.fsd.task.helper;

import com.fsd.task.model.IgracModel;
import com.fsd.task.service.FileStoreManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.core.annotation.HandleAfterDelete;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

import javax.annotation.Resource;

@RepositoryEventHandler
public class IgracEventHandler {

    @Resource
    private FileStoreManager fileStoreManager;

    private Logger LOG = LoggerFactory.getLogger(IgracEventHandler.class);

    @HandleAfterDelete
    public void handleIgracAfterDelete(IgracModel igrac) {
        LOG.info("Event handler: Deleting image...");
        fileStoreManager.delete(igrac.getSlika());
    }
}
