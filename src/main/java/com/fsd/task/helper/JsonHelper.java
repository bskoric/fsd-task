package com.fsd.task.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class JsonHelper {

    private static final Logger LOG = LoggerFactory.getLogger(JsonHelper.class);

    private static final String ROOT_NODE = "results";

    public static String createJson(List<ObjectNode> nodes) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();
            arrayNode.addAll(nodes);

            ObjectNode root = mapper.createObjectNode();
            root.set(ROOT_NODE, arrayNode);
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
        } catch (JsonProcessingException e) {
            LOG.error(e.getMessage());
            return StringUtils.EMPTY;
        }
    }
}
