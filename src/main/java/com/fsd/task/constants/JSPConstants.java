package com.fsd.task.constants;

public class JSPConstants {

    public static final String MESTO_JSP = "pages/mesto/mesto";
    public static final String KLUB_JSP = "pages/klub/klub";
    public static final String TAKMICENJE_JSP = "pages/takmicenje/takmicenje";
    public static final String IGRAC_JSP = "pages/igrac/igrac";
    public static final String NASTUP_JSP = "pages/nastup/nastup";
    public static final String NASTUP_USER_JSP = "pages/nastup/nastupUserView";

    public static final String ERROR_JSP = "pages/error/error";
}
