package com.fsd.task.config;

import com.fsd.task.helper.IgracEventHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RepositoryConfiguration {

    public RepositoryConfiguration() {
        super();
    }

    @Bean
    IgracEventHandler igracEventHandler() {
        return new IgracEventHandler();
    }
}
