package com.fsd.task.config;

import com.fsd.task.helper.CustomAuthenticationSuccessHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String ADMIN = "ADMIN";
    private static final String USER = "USER";

/*    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("user").password(passwordEncoder().encode("pass2")).roles(USER)
                .and()
                .withUser("admin").password(passwordEncoder().encode("pass1")).roles(ADMIN);
    }*/

    @Bean("authenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.formLogin().successHandler(customAuthenticationSuccessHandler());

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/mesto").hasRole(ADMIN)
                .antMatchers("/igrac").hasRole(ADMIN)
                .antMatchers("/klub").hasRole(ADMIN)
                .antMatchers("/takmicenje").hasRole(ADMIN)
                .antMatchers("/nastup").hasRole(ADMIN)
                .antMatchers("/nastup/1").hasRole(USER)
                .antMatchers("/login*").permitAll()
                .anyRequest().authenticated();

    }


    @Bean
    public AuthenticationSuccessHandler customAuthenticationSuccessHandler() {
        return new CustomAuthenticationSuccessHandler();
    }
}

