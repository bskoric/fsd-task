package com.fsd.task.service.impl;

import com.fsd.task.dao.KlubDao;
import com.fsd.task.model.KlubModel;
import com.fsd.task.service.KlubManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class KlubManagerImpl implements KlubManager {

    @Resource
    private KlubDao klubDao;

    @Override
    public List<KlubModel> getAll() {
        return klubDao.findAll();
    }

    @Override
    public Optional<KlubModel> getByID(int id) {
        return klubDao.findById(id);
    }

    @Override
    public KlubModel save(KlubModel entity) {
        return klubDao.save(entity);
    }

    @Override
    public void delete(int id) {
        klubDao.deleteById(id);
    }
}
