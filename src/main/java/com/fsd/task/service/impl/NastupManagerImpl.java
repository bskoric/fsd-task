package com.fsd.task.service.impl;

import com.fsd.task.dao.NastupDao;
import com.fsd.task.model.NastupModel;
import com.fsd.task.service.NastupManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class NastupManagerImpl implements NastupManager {

    @Resource
    private NastupDao nastupDao;

    @Override
    public List<NastupModel> getAll() {
        return nastupDao.findAll();
    }

    @Override
    public Optional<NastupModel> getByID(int id) {
        return nastupDao.findById(id);
    }

    @Override
    public NastupModel save(NastupModel entity) {
        return nastupDao.save(entity);
    }

    @Override
    public void delete(int id) {
        nastupDao.deleteById(id);
    }

    @Override
    public NastupModel getByIgracId(int igracId) {
        return nastupDao.findByIgracId(igracId);
    }
}
