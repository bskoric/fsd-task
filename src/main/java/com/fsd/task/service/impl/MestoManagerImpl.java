package com.fsd.task.service.impl;

import com.fsd.task.dao.MestoDao;
import com.fsd.task.model.MestoModel;
import com.fsd.task.service.MestoManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class MestoManagerImpl implements MestoManager {

    @Resource
    private MestoDao mestoDao;

    @Override
    public List<MestoModel> getAll() {
        return mestoDao.findAll();
    }

    @Override
    public Optional<MestoModel> getByID(int id) {
        return mestoDao.findById(id);
    }

    @Override
    public MestoModel save(MestoModel entity) {
        return mestoDao.save(entity);
    }

    @Override
    public void delete(int id) {
        mestoDao.deleteById(id);
    }
}
