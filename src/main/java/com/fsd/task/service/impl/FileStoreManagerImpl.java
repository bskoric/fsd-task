package com.fsd.task.service.impl;

import com.fsd.task.helper.ImagesStorageProperty;
import com.fsd.task.service.FileStoreManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@Service
public class FileStoreManagerImpl implements FileStoreManager {

    private final Path uploadPath;

    @Autowired
    public FileStoreManagerImpl(ImagesStorageProperty imagesStorageProperty) throws IOException {
        uploadPath = Paths.get(imagesStorageProperty.getUploadDirectory()).toAbsolutePath().normalize();
        Files.createDirectories(uploadPath);
    }

    @Override
    public void save(MultipartFile file) {
        try {
            Files.copy(file.getInputStream(), uploadPath.resolve(Objects.requireNonNull(file.getOriginalFilename())));
        } catch (IOException ex) {
            throw new RuntimeException("Problem sa uploadom fajla!");
        }
    }

    @Override
    public Resource load(String filename) {
        return null;
    }

    @Override
    public void delete(String fileName) {
        try {
            Files.delete(uploadPath.resolve(Objects.requireNonNull(fileName)));
        } catch (IOException e) {
            throw new RuntimeException("Problem sa brisanjem fajla");
        }
    }
}
