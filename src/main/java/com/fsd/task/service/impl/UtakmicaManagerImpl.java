package com.fsd.task.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fsd.task.dao.UtakmicaDao;
import com.fsd.task.helper.JsonHelper;
import com.fsd.task.model.UtakmicaModel;
import com.fsd.task.service.UtakmicaManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UtakmicaManagerImpl implements UtakmicaManager {

    private static final String ID = "id";
    private static final String TEXT = "text";

    @Resource
    private UtakmicaDao utakmicaDao;

    @Override
    public List<UtakmicaModel> getAll() {
        return utakmicaDao.findAll();
    }

    @Override
    public Optional<UtakmicaModel> getByID(int id) {
        return utakmicaDao.findById(id);
    }

    @Override
    public UtakmicaModel save(UtakmicaModel entity) {
        return utakmicaDao.save(entity);
    }

    @Override
    public void delete(int id) {
        utakmicaDao.deleteById(id);
    }

    @Override
    public String getForSelect() {
        return JsonHelper.createJson(createNodesObjects());
    }

    private List<ObjectNode> createNodesObjects() {
        List<ObjectNode> nodes = new ArrayList<>();
        for (UtakmicaModel utakmica : getAll()) {
            nodes.add(createJsonObject(utakmica));
        }
        return nodes;
    }

    private ObjectNode createJsonObject(UtakmicaModel utakmica) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode object = mapper.createObjectNode();
        object.put(ID, utakmica.getId());
        object.put(TEXT, getTextForSelect(utakmica));
        return object;
    }

    private String getTextForSelect(UtakmicaModel utakmica) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(utakmica.getDomacin().getNaziv());
        stringBuilder.append("-");
        stringBuilder.append(utakmica.getGost().getNaziv());
        stringBuilder.append("[");
        stringBuilder.append(utakmica.getTakmicenje().getNazivTakmicenja());
        stringBuilder.append(" ");
        stringBuilder.append(utakmica.getDatumOdigravanja());
        stringBuilder.append("]");
        return stringBuilder.toString();
    }
}
