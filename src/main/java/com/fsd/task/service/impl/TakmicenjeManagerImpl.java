package com.fsd.task.service.impl;

import com.fsd.task.dao.TakmicenjeDao;
import com.fsd.task.model.TakmicenjeModel;
import com.fsd.task.service.TakmicenjeManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class TakmicenjeManagerImpl implements TakmicenjeManager {

    @Resource
    private TakmicenjeDao takmicenjeDao;

    @Override
    public List<TakmicenjeModel> getAll() {
        return takmicenjeDao.findAll();
    }

    @Override
    public Optional<TakmicenjeModel> getByID(int id) {
        return takmicenjeDao.findById(id);
    }

    @Override
    public TakmicenjeModel save(TakmicenjeModel entity) {
        return takmicenjeDao.save(entity);
    }

    @Override
    public void delete(int id) {
        takmicenjeDao.deleteById(id);
    }
}
