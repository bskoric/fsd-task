package com.fsd.task.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fsd.task.dao.IgracDao;
import com.fsd.task.exceptions.EmptyFileException;
import com.fsd.task.helper.JsonHelper;
import com.fsd.task.model.IgracModel;
import com.fsd.task.service.FileStoreManager;
import com.fsd.task.service.IgracManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class IgracManagerImpl implements IgracManager {

    private Logger LOG = LoggerFactory.getLogger(IgracManagerImpl.class);

    private static final String ID = "id";
    private static final String TEXT = "text";

    @Resource
    private IgracDao igracDao;

    @Resource
    private FileStoreManager fileStoreManager;

    @Override
    public List<IgracModel> getAll() {
        return igracDao.findAll();
    }

    @Override
    public Optional<IgracModel> getByID(int id) {
        return igracDao.findById(id);
    }

    @Override
    public IgracModel save(IgracModel entity) {
        return igracDao.save(entity);
    }

    @Override
    public IgracModel saveEntityAndFile(IgracModel entity, MultipartFile file) throws EmptyFileException {
        if (Objects.isNull(file) || file.isEmpty()) {
            throw new EmptyFileException("File empty or null");
        }
        entity.setSlika(file.getOriginalFilename());
        fileStoreManager.save(file);
        return igracDao.save(entity);
    }

    @Override
    public String getForSelect() {
        return JsonHelper.createJson(createNodesObjects());
    }

    @Override
    public void delete(int id) {
        igracDao.deleteById(id);
    }

    private List<ObjectNode> createNodesObjects() {
        List<ObjectNode> nodes = new ArrayList<>();
        for (IgracModel igrac : getAll()) {
            nodes.add(createJsonObject(igrac));
        }
        return nodes;
    }

    private ObjectNode createJsonObject(IgracModel igrac) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode object = mapper.createObjectNode();
        object.put(ID, igrac.getId());
        object.put(TEXT, getTextForSelect(igrac));
        return object;
    }

    private String getTextForSelect(IgracModel igrac) {
        return igrac.getIme() + " " + igrac.getPrezime();
    }
}
