package com.fsd.task.service;

import com.fsd.task.model.NastupModel;

public interface NastupManager extends GenericManager<NastupModel> {
    NastupModel getByIgracId(int igracId);
}
