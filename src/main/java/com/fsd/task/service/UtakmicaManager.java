package com.fsd.task.service;

import com.fsd.task.model.UtakmicaModel;

import java.util.List;
import java.util.Optional;

public interface UtakmicaManager extends GenericManager<UtakmicaModel>{
    String getForSelect();
}
