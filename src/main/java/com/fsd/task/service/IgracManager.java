package com.fsd.task.service;

import com.fsd.task.model.IgracModel;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.NoSuchFileException;

public interface IgracManager extends GenericManager<IgracModel> {

    IgracModel saveEntityAndFile(IgracModel entity, MultipartFile file) throws NoSuchFileException;

    String getForSelect();
}
