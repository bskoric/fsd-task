package com.fsd.task.service;

import java.util.List;
import java.util.Optional;

public interface GenericManager<T> {

    List<T> getAll();
    Optional<T> getByID(int id);
    T save(T entity);
    void delete(int id);
}
