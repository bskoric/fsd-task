package com.fsd.task.controller;

import com.fsd.task.enums.PozicijaEnum;
import com.fsd.task.service.IgracManager;
import com.fsd.task.service.KlubManager;
import com.fsd.task.service.MestoManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;

import static com.fsd.task.constants.JSPConstants.IGRAC_JSP;

@Controller
public class IgracController {

    @Resource
    private IgracManager igracManager;

    @Resource
    private MestoManager mestoManager;

    @Resource
    private KlubManager klubManager;

    @GetMapping(value = "/igrac")
    public String showPage(Model model) {
        model.addAttribute("igraci", igracManager.getAll());
        model.addAttribute("mesta", mestoManager.getAll());
        model.addAttribute("klubovi", klubManager.getAll());
        model.addAttribute("pozicije", PozicijaEnum.values());
        return IGRAC_JSP;
    }
}
