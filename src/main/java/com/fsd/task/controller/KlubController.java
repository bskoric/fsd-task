package com.fsd.task.controller;

import com.fsd.task.service.KlubManager;
import com.fsd.task.service.MestoManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;

import static com.fsd.task.constants.JSPConstants.KLUB_JSP;

@Controller
public class KlubController {

    @Resource
    private KlubManager klubManager;

    @Resource
    private MestoManager mestoManager;

    @GetMapping(value = "/klub")
    public String showPage(Model model) {
        model.addAttribute("klubovi", klubManager.getAll());
        model.addAttribute("mesta", mestoManager.getAll());
        return KLUB_JSP;
    }
}
