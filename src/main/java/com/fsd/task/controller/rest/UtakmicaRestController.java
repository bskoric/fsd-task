package com.fsd.task.controller.rest;

import com.fsd.task.model.UtakmicaModel;
import com.fsd.task.service.UtakmicaManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/utakmica")
public class UtakmicaRestController {

    private Logger LOG = LoggerFactory.getLogger(UtakmicaRestController.class);

    @Resource
    private UtakmicaManager utakmicaManager;

    @GetMapping
    public List<UtakmicaModel> getAll() {
        return utakmicaManager.getAll();
    }

    @GetMapping(value = "/select", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getForSelect() {
        return ResponseEntity.ok(utakmicaManager.getForSelect());
    }
}
