package com.fsd.task.controller.rest;

import com.fsd.task.model.TakmicenjeModel;
import com.fsd.task.service.TakmicenjeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/takmicenje")
public class TakmicenjeRestController {

    private final Logger LOG = LoggerFactory.getLogger(TakmicenjeRestController.class);

    @Resource
    private TakmicenjeManager takmicenjeManager;

    @GetMapping
    public ResponseEntity<List<TakmicenjeModel>> getAll() {
        return ResponseEntity.ok(takmicenjeManager.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<TakmicenjeModel> getTakmicenje(@PathVariable int id) {
        Optional<TakmicenjeModel> takmicenjeOptional = takmicenjeManager.getByID(id);
        if (takmicenjeOptional.isPresent()) {
            return ResponseEntity.ok(takmicenjeOptional.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Takmicenje nije pronadjen");
        }
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TakmicenjeModel> createTakmicenje(@RequestBody @Valid TakmicenjeModel takmicenje) {
        LOG.info("Request to create takmicenje: {}", takmicenje);
        try {
            takmicenjeManager.save(takmicenje);
            return ResponseEntity.status(HttpStatus.CREATED).body(takmicenje);
        } catch (DataIntegrityViolationException integrityViolationException) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Vec postoji", integrityViolationException);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> deleteTakmicenje(@PathVariable int id) {
        takmicenjeManager.delete(id);
        return ResponseEntity.ok().build();
    }
}
