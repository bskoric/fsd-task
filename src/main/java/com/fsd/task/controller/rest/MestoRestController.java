package com.fsd.task.controller.rest;

import com.fsd.task.model.MestoModel;
import com.fsd.task.service.MestoManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@Validated
@RequestMapping("/api/mesto")
public class MestoRestController {

    private final Logger LOG = LoggerFactory.getLogger(MestoRestController.class);

    @Resource
    private MestoManager mestoManager;

    @Resource
    private MessageSource messageSource;

    @GetMapping
    public ResponseEntity<List<MestoModel>> getAll() {
        return ResponseEntity.ok(mestoManager.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<MestoModel> getMesto(@PathVariable int id) {
        Optional<MestoModel> mestoOptional = mestoManager.getByID(id);
        if (mestoOptional.isPresent()) {
            return ResponseEntity.ok(mestoOptional.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Mesto nije pronadjeno");
        }
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MestoModel> createMesto(@RequestBody @Valid MestoModel mesto) {
        LOG.info("Request to create mesto: {}", mesto);
        try {
            mestoManager.save(mesto);
            return ResponseEntity.status(HttpStatus.CREATED).body(mesto);
        } catch (DataIntegrityViolationException integrityViolationException) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "PTT kod vec postoji", integrityViolationException);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> deleteMesto(@PathVariable int id) {
        mestoManager.delete(id);
        return ResponseEntity.ok().build();
    }
}
