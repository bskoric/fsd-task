package com.fsd.task.controller.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsd.task.model.IgracModel;
import com.fsd.task.service.IgracManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.Resource;
import java.nio.file.NoSuchFileException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/igrac")
public class IgracRestController {

    private Logger LOG = LoggerFactory.getLogger(IgracRestController.class);

    @Resource
    private IgracManager igracManager;

    @GetMapping
    public List<IgracModel> getAll() {
        return igracManager.getAll();
    }

    @GetMapping(value = "/select", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getForSelect() {
        return ResponseEntity.ok(igracManager.getForSelect());
    }

    @GetMapping("/{id}")
    public ResponseEntity<IgracModel> getIgrac(@PathVariable int id) {
        Optional<IgracModel> igracOptional = igracManager.getByID(id);
        if (igracOptional.isPresent()) {
            return ResponseEntity.ok(igracOptional.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Igrac nije pronadjen");
        }
    }

    @PostMapping(consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<IgracModel> createIgrac(@RequestParam(required = false, name = "file") MultipartFile file, @RequestParam(name = "igrac", required = false) String igracJson) throws NoSuchFileException {
        LOG.info("Request to create mesto: {}", igracJson);
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            IgracModel igrac = objectMapper.readValue(igracJson, IgracModel.class);
            IgracModel createdIgrac = igracManager.saveEntityAndFile(igrac, file);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdIgrac);
        } catch (DataIntegrityViolationException integrityViolationException) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Vec postoji", integrityViolationException);
        } catch (JsonMappingException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Greska u parsiranju", e);
        } catch (JsonProcessingException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad json processing", e);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> deleteKlub(@PathVariable int id) {
        igracManager.delete(id);
        return ResponseEntity.ok().build();
    }
}
