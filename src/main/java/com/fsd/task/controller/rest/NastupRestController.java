package com.fsd.task.controller.rest;

import com.fsd.task.model.MestoModel;
import com.fsd.task.model.NastupModel;
import com.fsd.task.service.MestoManager;
import com.fsd.task.service.NastupManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@Validated
@RequestMapping("/api/nastup")
public class NastupRestController {

    private final Logger LOG = LoggerFactory.getLogger(NastupRestController.class);

    @Resource
    private NastupManager nastupManager;

    @GetMapping
    public ResponseEntity<List<NastupModel>> getAll() {
        return ResponseEntity.ok(nastupManager.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<NastupModel> getNastup(@PathVariable int id) {
        Optional<NastupModel> mestoOptional = nastupManager.getByID(id);
        if (mestoOptional.isPresent()) {
            return ResponseEntity.ok(mestoOptional.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Mesto nije pronadjeno");
        }
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<NastupModel> createNastup(@RequestBody @Valid NastupModel nastup) {
        LOG.info("Request to create mesto: {}", nastup);
        try {
            nastupManager.save(nastup);
            return ResponseEntity.status(HttpStatus.CREATED).body(nastup);
        } catch (DataIntegrityViolationException integrityViolationException) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Vec postoji", integrityViolationException);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> deleteMesto(@PathVariable int id) {
        nastupManager.delete(id);
        return ResponseEntity.ok().build();
    }
}
