package com.fsd.task.controller.rest;

import com.fsd.task.model.KlubModel;
import com.fsd.task.service.KlubManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/klub")
public class KlubRestController {

    private final Logger LOG = LoggerFactory.getLogger(KlubRestController.class);

    @Resource
    private KlubManager klubManager;

    @GetMapping
    public ResponseEntity<List<KlubModel>> getAll() {
        return ResponseEntity.ok(klubManager.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<KlubModel> getKlub(@PathVariable int id) {
        Optional<KlubModel> klubOptional = klubManager.getByID(id);
        if (klubOptional.isPresent()) {
            return ResponseEntity.ok(klubOptional.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Klub nije pronadjen");
        }
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KlubModel> createKlub(@RequestBody @Valid KlubModel klub) {
        LOG.info("Request to create mesto: {}", klub);
        try {
            klubManager.save(klub);
            return ResponseEntity.status(HttpStatus.CREATED).body(klub);
        } catch (DataIntegrityViolationException integrityViolationException) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Vec postoji", integrityViolationException);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> deleteKlub(@PathVariable int id) {
        klubManager.delete(id);
        return ResponseEntity.ok().build();
    }
}
