package com.fsd.task.controller.rest;

import com.fsd.task.data.RestErrorData;
import com.fsd.task.exceptions.EmptyFileException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import java.nio.file.NoSuchFileException;
import java.util.Objects;

@ControllerAdvice(annotations = RestController.class)
@Order(1)
public class RestExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        RestErrorData restErrorData = new RestErrorData();
        if (Objects.nonNull(ex.getFieldError())) {
            restErrorData.setMessage(StringUtils.defaultIfBlank(ex.getFieldError().getDefaultMessage(), ex.getMessage()));
            restErrorData.setStatus(HttpStatus.BAD_REQUEST.toString());
        }
        return ResponseEntity.badRequest().body(restErrorData);
    }

    @ExceptionHandler(EmptyFileException.class)
    public ResponseEntity<?> handleEmptyFileException(EmptyFileException ex) {
        RestErrorData restErrorData = new RestErrorData();
        restErrorData.setStatus(HttpStatus.BAD_REQUEST.toString());
        restErrorData.setMessage("Fajl prazan. Obavezno uneti sliku.");
        return ResponseEntity.badRequest().body(restErrorData);
    }
}
