package com.fsd.task.controller;

import com.fsd.task.service.MestoManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;

import static com.fsd.task.constants.JSPConstants.MESTO_JSP;

@Controller
public class MestoController {

    @Resource
    private MestoManager mestoManager;

    @GetMapping(value = "/mesto")
    public String showHome(Model model) {
        model.addAttribute("mesta", mestoManager.getAll());
        return MESTO_JSP;
    }
}
