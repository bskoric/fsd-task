package com.fsd.task.controller;

import com.fsd.task.service.TakmicenjeManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;

import static com.fsd.task.constants.JSPConstants.TAKMICENJE_JSP;

@Controller
public class TakmicenjeController {

    @Resource
    private TakmicenjeManager takmicenjeManager;

    @GetMapping(value = "/takmicenje")
    public String showPage(Model model) {
        model.addAttribute("takmicenja", takmicenjeManager.getAll());
        return TAKMICENJE_JSP;
    }
}
