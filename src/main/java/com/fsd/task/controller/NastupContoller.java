package com.fsd.task.controller;

import com.fsd.task.model.NastupModel;
import com.fsd.task.service.NastupManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.annotation.Resource;

import static com.fsd.task.constants.JSPConstants.NASTUP_JSP;
import static com.fsd.task.constants.JSPConstants.NASTUP_USER_JSP;

@Controller
public class NastupContoller {

    @Resource
    private NastupManager nastupManager;

    @GetMapping(value = "/nastup")
    public String showAdminPage(Model model) {
        model.addAttribute("nastupi", nastupManager.getAll());
        return NASTUP_JSP;
    }

    @GetMapping(value = "/nastup/{id}")
    public String showUserPage(Model model, @PathVariable int id) {
        NastupModel nastup = nastupManager.getByIgracId(id);
        model.addAttribute("nastup", nastup);
        return NASTUP_USER_JSP;

    }
}
