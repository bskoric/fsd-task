package com.fsd.task.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "nastup", schema = "fsd_zadatak")
public class NastupModel implements Serializable {
    private int id;
    private double ocenaIgraca;
    private IgracModel igrac;
    private UtakmicaModel utakmica;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "nastupID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ocenaIgraca")
    public double getOcenaIgraca() {
        return ocenaIgraca;
    }

    public void setOcenaIgraca(double ocenaIgraca) {
        this.ocenaIgraca = ocenaIgraca;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NastupModel that = (NastupModel) o;
        return Double.compare(that.ocenaIgraca, ocenaIgraca) == 0 && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ocenaIgraca);
    }

    @ManyToOne
    @JoinColumn(name = "igrac", referencedColumnName = "igracID", nullable = false)
    public IgracModel getIgrac() {
        return igrac;
    }

    public void setIgrac(IgracModel igrac) {
        this.igrac = igrac;
    }

    @ManyToOne
    @JoinColumn(name = "utakmica", referencedColumnName = "utakmicaID", nullable = false)
    public UtakmicaModel getUtakmica() {
        return utakmica;
    }

    public void setUtakmica(UtakmicaModel utakmica) {
        this.utakmica = utakmica;
    }
}
