package com.fsd.task.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "mesto", schema = "fsd_zadatak")
public class MestoModel implements Serializable {
    private int id;
    private int ptt;
    @NotBlank(message = "Naziv mesta ne moze biti prazan")
    private String naziv;
    @JsonIgnore
    private Collection<IgracModel> igraci;
    @JsonIgnore
    private Collection<KlubModel> klubovi;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "mestoID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ptt")
    public int getPtt() {
        return ptt;
    }

    public void setPtt(int ptt) {
        this.ptt = ptt;
    }

    @Basic
    @Column(name = "naziv")
    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MestoModel that = (MestoModel) o;
        return ptt == that.ptt && Objects.equals(id, that.id) && Objects.equals(naziv, that.naziv);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ptt, naziv);
    }

    @OneToMany(mappedBy = "mesto")
    public Collection<IgracModel> getIgraci() {
        return igraci;
    }

    public void setIgraci(Collection<IgracModel> igraci) {
        this.igraci = igraci;
    }

    @OneToMany(mappedBy = "mesto")
    public Collection<KlubModel> getKlubovi() {
        return klubovi;
    }

    public void setKlubovi(Collection<KlubModel> klubovi) {
        this.klubovi = klubovi;
    }

    @Override
    public String toString() {
        return "MestoModel{" +
                "id=" + id +
                ", ptt=" + ptt +
                ", naziv='" + naziv + '\'' +
                '}';
    }
}
