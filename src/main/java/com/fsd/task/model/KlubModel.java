package com.fsd.task.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "klub", schema = "fsd_zadatak")
public class KlubModel implements Serializable {
    private int id;
    @NotBlank(message = "Naziv kluba ne moze biti prazan")
    private String naziv;
    @JsonIgnore
    private Collection<IgracModel> igraci;
    private MestoModel mesto;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "klubID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "naziv")
    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KlubModel klubModel = (KlubModel) o;
        return Objects.equals(id, klubModel.id) && Objects.equals(naziv, klubModel.naziv);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, naziv);
    }

    @OneToMany(mappedBy = "klub")
    public Collection<IgracModel> getIgraci() {
        return igraci;
    }

    public void setIgraci(Collection<IgracModel> igraci) {
        this.igraci = igraci;
    }

    @ManyToOne
    @JoinColumn(name = "mesto", referencedColumnName = "mestoID", nullable = false)
    public MestoModel getMesto() {
        return mesto;
    }

    public void setMesto(MestoModel mesto) {
        this.mesto = mesto;
    }
}
