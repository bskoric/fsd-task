package com.fsd.task.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "utakmica", schema = "fsd_zadatak")
public class UtakmicaModel implements Serializable {
    private int id;
    private Date datumOdigravanja;
    private KlubModel domacin;
    private KlubModel gost;
    private TakmicenjeModel takmicenje;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "utakmicaID")
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "datumOdigravanja")
    public Date getDatumOdigravanja() {
        return datumOdigravanja;
    }

    public void setDatumOdigravanja(Date datumOdigravanja) {
        this.datumOdigravanja = datumOdigravanja;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UtakmicaModel that = (UtakmicaModel) o;
        return Objects.equals(id, that.id) && Objects.equals(datumOdigravanja, that.datumOdigravanja);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, datumOdigravanja);
    }

    @ManyToOne
    @JoinColumn(name = "domacin", referencedColumnName = "klubID", nullable = false)
    public KlubModel getDomacin() {
        return domacin;
    }

    public void setDomacin(KlubModel domacin) {
        this.domacin = domacin;
    }

    @ManyToOne
    @JoinColumn(name = "gost", referencedColumnName = "klubID", nullable = false)
    public KlubModel getGost() {
        return gost;
    }

    public void setGost(KlubModel gost) {
        this.gost = gost;
    }

    @ManyToOne
    @JoinColumn(name = "takmicenje", referencedColumnName = "takmicenjeID", nullable = false)
    public TakmicenjeModel getTakmicenje() {
        return takmicenje;
    }

    public void setTakmicenje(TakmicenjeModel takmicenje) {
        this.takmicenje = takmicenje;
    }
}
