package com.fsd.task.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fsd.task.enums.PozicijaEnum;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "igrac", schema = "fsd_zadatak")
public class IgracModel implements Serializable {

    private int id;

    @NotBlank(message = "Ime je obavezan")
    private String ime;

    @NotBlank(message = "Prezime je obavezano")
    private String prezime;

    @NotBlank(message = "JMBG je obavezan")
    @Pattern(regexp = "^[0-9]{13}$", message = "JMBG mora imati 13 cifara")
    private String jmbg;

    private Date datumRodj;

    @NotNull(message = "Pozicija je obavezna")
    private PozicijaEnum pozicija;

    private KlubModel klub;

    private MestoModel mesto;

    private String slika;

    @JsonIgnore
    private Collection<NastupModel> nastupi;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "igracID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ime")
    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    @Basic
    @Column(name = "prezime")
    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    @Basic
    @Column(name = "JMBG", unique = true)
    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }

    @Basic
    @Column(name = "datumRodj")
    public Date getDatumRodj() {
        return datumRodj;
    }

    public void setDatumRodj(Date datumRodj) {
        this.datumRodj = datumRodj;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "pozicija")
    public PozicijaEnum getPozicija() {
        return pozicija;
    }

    public void setPozicija(PozicijaEnum pozicija) {
        this.pozicija = pozicija;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IgracModel that = (IgracModel) o;
        return Objects.equals(id, that.id) && Objects.equals(ime, that.ime) && Objects.equals(prezime, that.prezime) && Objects.equals(jmbg, that.jmbg) && Objects.equals(datumRodj, that.datumRodj) && Objects.equals(pozicija, that.pozicija);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ime, prezime, jmbg, datumRodj, pozicija);
    }

    @ManyToOne
    @JoinColumn(name = "klub", referencedColumnName = "klubID", nullable = false)
    public KlubModel getKlub() {
        return klub;
    }

    public void setKlub(KlubModel klub) {
        this.klub = klub;
    }

    @ManyToOne
    @JoinColumn(name = "mesto", referencedColumnName = "mestoID", nullable = false)
    public MestoModel getMesto() {
        return mesto;
    }

    public void setMesto(MestoModel mesto) {
        this.mesto = mesto;
    }

    @OneToMany(mappedBy = "igrac")
    public Collection<NastupModel> getNastupi() {
        return nastupi;
    }

    public void setNastupi(Collection<NastupModel> nastupi) {
        this.nastupi = nastupi;
    }

    @Basic
    @Type(type = "text")
    @Column(name = "slika")
    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }
}
