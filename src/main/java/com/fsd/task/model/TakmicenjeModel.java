package com.fsd.task.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "takmicenje", schema = "fsd_zadatak")
public class TakmicenjeModel implements Serializable {
    private int id;
    @NotBlank(message = "Naziv takmicenja je obavezan")
    private String nazivTakmicenja;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "takmicenjeID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nazivTakmicenja")
    public String getNazivTakmicenja() {
        return nazivTakmicenja;
    }

    public void setNazivTakmicenja(String nazivTakmicenja) {
        this.nazivTakmicenja = nazivTakmicenja;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TakmicenjeModel that = (TakmicenjeModel) o;
        return Objects.equals(id, that.id) && Objects.equals(nazivTakmicenja, that.nazivTakmicenja);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nazivTakmicenja);
    }
}
