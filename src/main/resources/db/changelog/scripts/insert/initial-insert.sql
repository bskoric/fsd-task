INSERT INTO `mesto` (`mestoID`, `ptt`, `naziv`) VALUES
(1, 11000, 'Beograd'),
(2, 15314, 'Krupanj');

INSERT INTO `klub` (`klubID`, `naziv`, `mesto`) VALUES
(1, 'Crvena Zvezda', 1),
(2, 'Partizan', 1);

INSERT INTO `igrac` (`igracID`, `ime`, `prezime`, `JMBG`, `datumRodj`, `pozicija`, `slika`, `klub`, `mesto`) VALUES
(1, 'Aleksandar', 'Katai', '1234567891234', '1984-11-12', 'CF', 'Screenshot.png', 1, 1),
(2, 'Njegos', 'Petrovic', '1234567891233', '1999-11-12', 'CMF', 'Screenshot.png', 1, 2);

INSERT INTO `takmicenje` (`takmicenjeID`, `nazivTakmicenja`) VALUES
(1, 'Kup Srbije');

INSERT INTO `utakmica` (`utakmicaID`, `datumOdigravanja`, `domacin`, `gost`, `takmicenje`) VALUES
(1, '2021-02-11', 1, 2, 1);

INSERT INTO `nastup` (`nastupID`, `igrac`, `utakmica`, `ocenaIgraca`) VALUES
(1, 1, 1, 5);