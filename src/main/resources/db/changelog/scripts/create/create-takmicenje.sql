--
-- Table structure for table `takmicenje`
--

CREATE TABLE IF NOT EXISTS `takmicenje` (
    `takmicenjeID` int(11) NOT NULL AUTO_INCREMENT,
    `nazivTakmicenja` varchar(255) NOT NULL,
    PRIMARY KEY (takmicenjeID)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;