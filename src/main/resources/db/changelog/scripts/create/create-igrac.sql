-- Table structure for table `Igrac`
--

CREATE TABLE IF NOT EXISTS `igrac` (
    `igracID` int(10) NOT NULL AUTO_INCREMENT,
    `ime` varchar(255) NOT NULL,
    `prezime` varchar(255) NOT NULL,
    `JMBG` varchar(13) NOT NULL,
    `datumRodj` date NOT NULL,
    `pozicija` varchar(100) NOT NULL,
    `slika` text,
    `klub` int(10) NOT NULL,
    `mesto` int(10) NOT NULL,
    PRIMARY KEY (igracID)
    ) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Indexes for table `Igrac`
--
ALTER TABLE `igrac`
  ADD UNIQUE KEY `jmbg_unique` (`JMBG`),
  ADD KEY `mesto` (`mesto`),
  ADD KEY `klub_index` (`klub`);

--
-- Constraints for table `Igrac`
--
ALTER TABLE `igrac`
    ADD CONSTRAINT `igrac_fk_klub` FOREIGN KEY (`klub`) REFERENCES `klub` (`klubID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `igrac_fk_mesto` FOREIGN KEY (`mesto`) REFERENCES `mesto` (`mestoID`) ON UPDATE CASCADE;