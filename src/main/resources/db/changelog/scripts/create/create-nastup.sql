--
-- Table structure for table `nastup`
--

CREATE TABLE IF NOT EXISTS `nastup` (
    `nastupID` int(10) NOT NULL AUTO_INCREMENT,
    `igrac` int(10) NOT NULL,
    `utakmica` int(10) NOT NULL,
    `ocenaIgraca` double NOT NULL,
    PRIMARY KEY (`nastupID`,`igrac`,`utakmica`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `nastup`
  ADD UNIQUE KEY `igrac` (`igrac`,`utakmica`),
    ADD KEY `nastup_fk_utakmica` (`utakmica`);

ALTER TABLE `nastup`
  ADD CONSTRAINT `nastup_fk_igrac` FOREIGN KEY (`igrac`) REFERENCES `igrac` (`igracID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nastup_fk_utakmica` FOREIGN KEY (`utakmica`) REFERENCES `utakmica` (`utakmicaID`) ON DELETE CASCADE ON UPDATE CASCADE;