--
-- Table structure for table `klub`
--

CREATE TABLE IF NOT EXISTS `klub` (
    `klubID` int(10) NOT NULL AUTO_INCREMENT,
    `naziv` varchar(255) NOT NULL,
    `mesto` int(10) NOT NULL,
    PRIMARY KEY (klubID)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    --
    -- Indexes for table `klub`
    --
    ALTER TABLE `klub`
      ADD KEY `mesto_index` (`mesto`);

      --
      -- Constraints for table `klub`
      --
      ALTER TABLE `klub`
          ADD CONSTRAINT `klub_fk_mesto` FOREIGN KEY (`mesto`) REFERENCES `mesto` (`mestoID`) ON UPDATE CASCADE;