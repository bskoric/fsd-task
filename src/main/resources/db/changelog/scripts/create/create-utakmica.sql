--
-- Table structure for table `utakmica`
--

CREATE TABLE IF NOT EXISTS `utakmica` (
    `utakmicaID` int(10) NOT NULL AUTO_INCREMENT,
    `datumOdigravanja` date NOT NULL,
    `domacin` int(10) NOT NULL,
    `gost` int(10) NOT NULL,
    `takmicenje` int(10) NOT NULL,
    PRIMARY KEY (utakmicaID)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `utakmica`
--
ALTER TABLE `utakmica`
  ADD KEY `domacin_klub_index` (`domacin`),
  ADD KEY `gost_klub_index` (`gost`),
  ADD KEY `takmicenje_index` (`takmicenje`);

--
-- Constraints for table `utakmica`
--
ALTER TABLE `utakmica`
    ADD CONSTRAINT `utakmica_fk_domaci` FOREIGN KEY (`domacin`) REFERENCES `klub` (`klubID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `utakmica_fk_gost` FOREIGN KEY (`gost`) REFERENCES `klub` (`klubID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `utakmica_fk_takmicenje` FOREIGN KEY (`takmicenje`) REFERENCES `takmicenje` (`takmicenjeID`) ON DELETE CASCADE ON UPDATE CASCADE;