
--
-- Table structure for table `mesto`
--

CREATE TABLE IF NOT EXISTS `mesto` (
    `mestoID` int(11) NOT NULL AUTO_INCREMENT,
    `ptt` int(11) NOT NULL,
    `naziv` varchar(255) NOT NULL,
    PRIMARY KEY (mestoID)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;