function printAlert(msg) {
    $("#resultMessageText").text(msg);
    $('#resultMessage').css("display", "inherit");
}

$('#resultMessageClose').on('click', function (e) {
    $('#resultMessage').css("display", "none");
});