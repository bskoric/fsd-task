<%--
  Created by IntelliJ IDEA.
  User: branko
  Date: 13.2.21.
  Time: 13:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE>
<html lang="rs">
<head>
    <title>FSD Klub</title>
</head>
<script src="/resources/js/moment.min.js"></script>
<body>
<jsp:include page="../../nav/nav.jsp"/>

<div style="width: 90%; margin: auto;">
    <section class="dark-grey-text text-center">

        <%@include file="../../helpers/alert.html" %>

        <h3 class="font-weight-bold pt-4 mb-4">Igraci</h3>

        <h5 class="font-weight-bold font-italic mb-5">At vero eos et accusamus et iusto odio dignissimos qui cupiditate
            non provident.</h5>

        <div class="row">
            <div class="col-md-4 mb-4">
                <p class="h4 mb-4">Unos</p>

                <form method="post" id="insertForm">
                    <input type="number" id="idInput" name="id" class="form-control" hidden>

                    <div class="col-md-12 mb-4">
                        <label>Ime</label>
                        <input type="text" id="imeInput" name="ime" class="form-control"
                               placeholder="Unesite ime igraca" required
                               oninvalid="this.setCustomValidity('<fmt:message key="required.field"/>')"
                               onchange="this.setCustomValidity('')"
                        />
                    </div>
                    <div class="col-md-12 mb-4">
                        <label>Prezime</label>
                        <input type="text" id="prezimeInput" name="prezime" class="form-control"
                               placeholder="Unesite prezime igraca" required
                               oninvalid="this.setCustomValidity('<fmt:message key="required.field"/>')"
                               onchange="this.setCustomValidity('')"
                        />
                    </div>
                    <div class="col-md-12 mb-4">
                        <label>JMBG</label>
                        <input type="text" id="jmbgInput" name="jmbg" class="form-control"
                               placeholder="JMBG" required
                               oninvalid="this.setCustomValidity('<fmt:message key="required.field"/>')"
                               onchange="this.setCustomValidity('')"
                        />
                    </div>

                    <div class="col-md-12 mb-4">
                        <label>Datum rodjenja</label>
                        <input type="date" id="datumInput" name="datum" class="form-control"
                               required
                               oninvalid="this.setCustomValidity('<fmt:message key="required.field"/>')"
                               onchange="this.setCustomValidity('')"
                        />
                    </div>

                    <div class="col-md-12 mb-4">
                        <label>Izaberite poziciju</label>
                        <select class="browser-default custom-select" id="pozicijaSelect">
                            <c:forEach var="pozicija" items="${pozicije}" varStatus="status">
                                <option value="${pozicija}">${pozicija}</option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="col-md-12 mb-4">
                        <label>Izaberite mesto</label>
                        <select class="browser-default custom-select" id="mestoSelect">
                            <c:forEach var="mesto" items="${mesta}" varStatus="status">
                                <option value="${mesto.id}">${mesto.naziv}</option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="col-md-12 mb-4">
                        <label>Izaberite klub</label>
                        <select class="browser-default custom-select" id="klubSelect">
                            <c:forEach var="klub" items="${klubovi}" varStatus="status">
                                <option value="${klub.id}">${klub.naziv} (${klub.mesto.naziv})</option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="input-group col-md-12 mb-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="slikaInput"
                                   aria-describedby="slikaInput"
                                   required
                                   oninvalid="this.setCustomValidity('<fmt:message key="required.field"/>')"
                                   onchange="this.setCustomValidity('')">
                            <label class="custom-file-label" for="slikaInput">Choose file</label>
                        </div>
                    </div>

                    <div class="col-md-12 mb-4">
                        <button type="submit" name="igracSubmit" id="igracSubmit" class="btn btn-default">Unos</button>
                    </div>
                </form>
            </div>

            <div class="col-md-8 mb-4">
                <div class="table-responsive text-nowrap">
                    <table class="table table-striped" id="igracTable">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Ime</th>
                            <th scope="col">Prezime</th>
                            <th scope="col">JMBG</th>
                            <th scope="col">Datum rodjenja</th>
                            <th scope="col">Pozicija</th>
                            <th scope="col">Klub</th>
                            <th scope="col">Mesto</th>
                            <th scope="col">Slika</th>
                            <th scope="col">Akcija</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="igrac" items="${igraci}" varStatus="status">
                            <tr id="${igrac.id}">
                                <td>${igrac.ime}</td>
                                <td>${igrac.prezime}</td>
                                <td>${igrac.jmbg}</td>
                                <td>${igrac.datumRodj}</td>
                                <td>${igrac.pozicija}</td>
                                <td>${igrac.klub.naziv} (${igrac.klub.mesto.naziv})</td>
                                <td>${igrac.mesto.naziv}</td>
                                <td>${igrac.slika}</td>
                                <td>
                                    <button type="submit" name="update" id="${igrac.id}"
                                            class="btn btn-default btn-md igracUpdate">
                                        <i class="far fa-edit"></i>
                                    </button>
                                    <button type="submit" name="delete" id="${igrac.id}"
                                            class="btn btn-danger btn-md igracDelete">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
</div>
<script>
    $(document).ready(function () {
        var table = $('#igracTable')

        // DATATABLE INIT
        table.DataTable();

        // INSERT AND UPDATE TABLE
        $('#insertForm').submit(function (e) {
            e.preventDefault();

            igracJSON = JSON.stringify({
                "id": $('#idInput').val(),
                "ime": $('#imeInput').val(),
                "prezime": $('#prezimeInput').val(),
                "jmbg": $('#jmbgInput').val(),
                "datumRodj": $('#datumInput').val(),
                "pozicija": $('#pozicijaSelect').val(),
                "klub": {
                    "id": $('#klubSelect').val()
                },
                "mesto": {
                    "id": $('#mestoSelect').val()
                }
            });
            var formData = new FormData();
            var images = $("#slikaInput")[0].files[0];
            if (!images) {
                printAlert("<fmt:message key="empty.file.image"/>")
                return;
            }
            formData.append("file", images);
            formData.append("igrac", igracJSON);
            $.ajax({
                url: '${pageContext.request.contextPath}/api/igrac',
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                method: 'POST',
                data: formData,
                success: function (data) {
                    printAlert('<fmt:message key="successful.insert"/>');
                    populateForm("", "", "1");
                    $.ajax({
                        'url': "${pageContext.request.contextPath}/api/igrac",
                        'method': "GET",
                        'contentType': 'application/json'
                    }).done(function (data) {
                        table.DataTable().clear();
                        table.DataTable().destroy();
                        table.DataTable({
                            "data": data,
                            "rowId": "id",
                            "columns": [
                                {"data": "ime"},
                                {"data": "prezime"},
                                {"data": "jmbg"},
                                {"data": "datumRodj"},
                                {"data": "pozicija"},
                                {
                                    "data": '',
                                    render: function (data, type, row) {
                                        return row.klub.naziv + " (" + row.klub.mesto.naziv + ")";
                                    }
                                },
                                {"data": "mesto.naziv"},
                                {"data": "slika"},
                                {
                                    "data": '',
                                    render: function (data, type, row) {
                                        return '<button type="submit" name="update" id="' + row.id + '" class="btn btn-default btn-md igracUpdate"><i class="far fa-edit"></i></button>' +
                                            '<button type="submit" name="delete" id="' + row.id + '"  class="btn btn-danger btn-md igracDelete"><i class="far fa-trash-alt"></i></button>'
                                    }
                                }
                            ]
                        })
                    })

                },
                error: function (error) {
                    printAlert(error.responseJSON.message);
                }
            });
        });

        // GET MESTO FOR UPDATE
        $(document).on('click', '.igracUpdate', function (e) {
            e.preventDefault();
            $.ajax({
                url: '${pageContext.request.contextPath}/api/igrac/' + this.id,
                method: 'GET',
                success: function (data) {
                    populateForm(data.id, data.ime, data.prezime, data.jmbg, data.datumRodj, data.pozicija, data.klub.id, data.mesto.id);
                }
            })
        });

        // DELETE MESTO
        $(document).on('click', '.igracDelete', function (e) {
            e.preventDefault();
            var confirmation = confirm('<fmt:message key="confirmation.delete"/>');
            var deleteId = this.id;
            if (confirmation) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/api/igrac/' + deleteId,
                    method: 'DELETE',
                    success: function (data) {
                        printAlert('<fmt:message key='successful.delete'/>');
                        $('#igracTable #' + deleteId).remove();
                    }
                })
            }
        });

    });

    function populateForm(id, ime, prezime, jmbg, datumRodj, pozicija, klubId, mestoId) {
        $('#idInput').val(id);
        $('#imeInput').val(ime);
        $('#prezimeInput').val(prezime);
        $('#jmbgInput').val(jmbg);
        $('#datumInput').val(moment(datumRodj).format('DD-MM-YYYY'));
        $('#pozicijaSelect option[value=' + pozicija + ']').attr('selected', 'selected');
        $('#mestoSelect option[value=' + mestoId + ']').attr('selected', 'selected');
        $('#klubSelect option[value=' + klubId + ']').attr('selected', 'selected');
    }
</script>
</body>
</html>
