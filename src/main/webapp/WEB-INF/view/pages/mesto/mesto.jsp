<%--
  Created by IntelliJ IDEA.
  User: branko
  Date: 13.2.21.
  Time: 13:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE>
<html lang="rs">
<head>
    <title>FSD Mesto</title>
</head>
<body>
<jsp:include page="../../nav/nav.jsp"/>

<div style="width: 90%; margin: auto;">
    <section class="dark-grey-text text-center">

        <%@include file="../../helpers/alert.html" %>

        <h3 class="font-weight-bold pt-4 mb-4">Mesto</h3>

        <h5 class="font-weight-bold font-italic mb-5">At vero eos et accusamus et iusto odio dignissimos qui cupiditate
            non provident.</h5>

        <div class="row">
            <div class="col-md-4 mb-4">
                <p class="h4 mb-4">Unos</p>

                <form method="post" id="mestoForm">
                    <input type="number" id="idInput" name="ptt" class="form-control" hidden>

                    <div class="col-md-12 mb-4">
                        <input type="number" id="pttInput" name="ptt" class="form-control" placeholder="PTT" required
                               oninvalid="this.setCustomValidity('<fmt:message key="required.field"/>')"
                               onchange="this.setCustomValidity('')"
                        >
                    </div>

                    <div class="col-md-12 mb-4">
                        <input type="text" path="" id="nazivInput" name="naziv" class="form-control" placeholder="Naziv"
                               required
                               oninvalid="this.setCustomValidity('Obavezno polje')"
                               onchange="this.setCustomValidity('')"
                        />
                        <sf:errors path="naziv" cssClass="error"/>
                    </div>

                    <div class="col-md-12 mb-4">
                        <button type="submit" name="mestoSubmit" id="mestoSubmit" class="btn btn-default">Unos</button>
                    </div>
                </form>
            </div>

            <div class="col-md-8 mb-4">
                <div class="table-responsive text-nowrap">
                    <table class="table table-striped" id="mestoTable">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">PTT</th>
                            <th scope="col">Naziv</th>
                            <th scope="col">Akcija</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="mesto" items="${mesta}" varStatus="status">
                            <tr id="${mesto.id}">
                                <td>${mesto.ptt}</td>
                                <td>${mesto.naziv}</td>
                                <td>
                                    <button type="submit" name="update" id="${mesto.id}"
                                            class="btn btn-default btn-md mestoUpdate">
                                        <i class="far fa-edit"></i>
                                    </button>
                                    <button type="submit" name="delete" id="${mesto.id}"
                                            class="btn btn-danger btn-md mestoDelete">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
</div>
<script>
    $(document).ready(function () {
        // DATATABLE INIT
        $('#mestoTable').DataTable();

        // INSERT AND UPDATE TABLE
        $('#mestoForm').submit(function (e) {
            e.preventDefault();

            mestoJSON = JSON.stringify({
                "id": $('#idInput').val(),
                "ptt": $('#pttInput').val(),
                "naziv": $('#nazivInput').val()
            });

            $.ajax({
                url: '${pageContext.request.contextPath}/api/mesto',
                headers: {'Content-Type': 'application/json'},
                method: 'POST',
                data: mestoJSON,
                success: function (data) {
                    printAlert('<fmt:message key="successful.insert"/>');
                    populateForm("", "", "");
                    $.ajax({
                        'url': "${pageContext.request.contextPath}/api/mesto",
                        'method': "GET",
                        'contentType': 'application/json'
                    }).done(function (data) {
                        $('#mestoTable').DataTable().clear();
                        $('#mestoTable').DataTable().destroy();
                        $('#mestoTable').DataTable({
                            "data": data,
                            "rowId": "id",
                            "columns": [
                                {"data": "ptt"},
                                {"data": "naziv"},
                                {
                                    "data": '',
                                    render: function (data, type, row) {
                                        return '<button type="submit" name="update" id="' + row.id + '" class="btn btn-default btn-md mestoUpdate"><i class="far fa-edit"></i></button>' +
                                            '<button type="submit" name="delete" id="' + row.id + '"  class="btn btn-danger btn-md mestoDelete"><i class="far fa-trash-alt"></i></button>'
                                    }
                                }
                            ]
                        })
                    })

                },
                error: function (error) {
                    printAlert(error.responseJSON.message);
                }
            });
        });

        // GET MESTO FOR UPDATE
        $(document).on('click', '.mestoUpdate', function (e) {
            e.preventDefault();
            console.log(this.id)
            $.ajax({
                url: '${pageContext.request.contextPath}/api/mesto/' + this.id,
                method: 'GET',
                success: function (data) {
                    populateForm(data.id, data.ptt, data.naziv);
                }
            })
        });

        // DELETE MESTO
        $(document).on('click', '.mestoDelete', function (e) {
            e.preventDefault();
            var confirmation = confirm('<fmt:message key="confirmation.delete"/>');
            var deleteId = this.id;
            if (confirmation) {
            $.ajax({
                url: '${pageContext.request.contextPath}/api/mesto/' + deleteId,
                method: 'DELETE',
                success: function (data) {
                    printAlert('<fmt:message key='successful.delete'/>');
                    $('#mestoTable #' + deleteId).remove();
                }
            })
            }
        });

    });

    function populateForm(id, ptt, naziv) {
        $('#idInput').val(id);
        $('#pttInput').val(ptt);
        $('#nazivInput').val(naziv);
    }
</script>
</body>
</html>
