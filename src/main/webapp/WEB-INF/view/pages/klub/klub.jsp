<%--
  Created by IntelliJ IDEA.
  User: branko
  Date: 13.2.21.
  Time: 13:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE>
<html lang="rs">
<head>
    <title>FSD Klub</title>
</head>
<body>
<jsp:include page="../../nav/nav.jsp"/>

<div style="width: 90%; margin: auto;">
    <section class="dark-grey-text text-center">

        <%@include file="../../helpers/alert.html" %>

        <h3 class="font-weight-bold pt-4 mb-4">Klub</h3>

        <h5 class="font-weight-bold font-italic mb-5">At vero eos et accusamus et iusto odio dignissimos qui cupiditate
            non provident.</h5>

        <div class="row">
            <div class="col-md-4 mb-4">
                <p class="h4 mb-4">Unos</p>

                <form method="post" id="insertForm">
                    <input type="number" id="idInput" name="id" class="form-control" hidden>

                    <div class="col-md-12 mb-4">
                        <input type="text" id="nazivInput" name="naziv" class="form-control"
                               placeholder="Unesite naziv kluba" required
                               oninvalid="this.setCustomValidity('<fmt:message key="required.field"/>')"
                               onchange="this.setCustomValidity('')"
                        />
                    </div>

                    <div class="col-md-12 mb-4">
                        <select class="browser-default custom-select" id="mestoSelect">
                            <c:forEach var="mesto" items="${mesta}" varStatus="status">
                                <option value="${mesto.id}">${mesto.naziv}</option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="col-md-12 mb-4">
                        <button type="submit" name="klubSubmit" id="klubSubmit" class="btn btn-default">Unos</button>
                    </div>
                </form>
            </div>

            <div class="col-md-8 mb-4">
                <div class="table-responsive text-nowrap">
                    <table class="table table-striped" id="klubTable">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Naziv</th>
                            <th scope="col">Mesto</th>
                            <th scope="col">Akcija</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="klub" items="${klubovi}" varStatus="status">
                            <tr id="${klub.id}">
                                <td>${klub.naziv}</td>
                                <td>${klub.mesto.naziv}</td>
                                <td>
                                    <button type="submit" name="update" id="${klub.id}"
                                            class="btn btn-default btn-md klubUpdate">
                                        <i class="far fa-edit"></i>
                                    </button>
                                    <button type="submit" name="delete" id="${klub.id}"
                                            class="btn btn-danger btn-md klubDelete">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
</div>
<script>
    $(document).ready(function () {
        var table = $('#klubTable')

        // DATATABLE INIT
        table.DataTable();

        // INSERT AND UPDATE TABLE
        $('#insertForm').submit(function (e) {
            e.preventDefault();

            klubJSON = JSON.stringify({
                "id": $('#idInput').val(),
                "naziv": $('#nazivInput').val(),
                "mesto": {
                    "id": $('#mestoSelect').val()
                },
            });

            $.ajax({
                url: '${pageContext.request.contextPath}/api/klub',
                headers: {'Content-Type': 'application/json'},
                method: 'POST',
                data: klubJSON,
                success: function (data) {
                    printAlert('<fmt:message key="successful.insert"/>');
                    populateForm("", "","1");
                    $.ajax({
                        'url': "${pageContext.request.contextPath}/api/klub",
                        'method': "GET",
                        'contentType': 'application/json'
                    }).done(function (data) {
                        table.DataTable().clear();
                        table.DataTable().destroy();
                        table.DataTable({
                            "data": data,
                            "rowId": "id",
                            "columns": [
                                {"data": "naziv"},
                                {"data": "mesto.naziv"},
                                {
                                    "data": '',
                                    render: function (data, type, row) {
                                        return '<button type="submit" name="update" id="' + row.id + '" class="btn btn-default btn-md klubUpdate"><i class="far fa-edit"></i></button>' +
                                            '<button type="submit" name="delete" id="' + row.id + '"  class="btn btn-danger btn-md klubDelete"><i class="far fa-trash-alt"></i></button>'
                                    }
                                }
                            ]
                        })
                    })

                },
                error: function (error) {
                    printAlert(error.responseJSON.message);
                }
            });
        });

        // GET MESTO FOR UPDATE
        $(document).on('click', '.klubUpdate', function (e) {
            e.preventDefault();
            $.ajax({
                url: '${pageContext.request.contextPath}/api/klub/' + this.id,
                method: 'GET',
                success: function (data) {
                    populateForm(data.id, data.naziv, data.mesto.id);
                }
            })
        });

        // DELETE MESTO
        $(document).on('click', '.klubDelete', function (e) {
            e.preventDefault();
            var confirmation = confirm('<fmt:message key="confirmation.delete"/>');
            var deleteId = this.id;
            if (confirmation) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/api/klub/' + deleteId,
                    method: 'DELETE',
                    success: function (data) {
                        printAlert('<fmt:message key='successful.delete'/>');
                        $('#klubTable #' + deleteId).remove();
                    }
                })
            }
        });

    });

    function populateForm(id, naziv, mestoID) {
        $('#idInput').val(id);
        $('#nazivInput').val(naziv);
        console.log(mestoID);
        $('#mestoSelect option[value=' + mestoID + ']').attr('selected', 'selected');
    }
</script>
</body>
</html>
