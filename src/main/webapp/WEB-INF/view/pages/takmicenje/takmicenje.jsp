<%--
  Created by IntelliJ IDEA.
  User: branko
  Date: 13.2.21.
  Time: 13:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE>
<html lang="rs">
<head>
    <title>FSD Takmicenje</title>
</head>
<body>
<jsp:include page="../../nav/nav.jsp"/>

<div style="width: 90%; margin: auto;">
    <section class="dark-grey-text text-center">

        <%@include file="../../helpers/alert.html" %>

        <h3 class="font-weight-bold pt-4 mb-4">Takmicenje</h3>

        <h5 class="font-weight-bold font-italic mb-5">At vero eos et accusamus et iusto odio dignissimos qui cupiditate
            non provident.</h5>

        <div class="row">
            <div class="col-md-4 mb-4">
                <p class="h4 mb-4">Unos</p>

                <form method="post" id="insertForm">
                    <input type="number" id="idInput" name="id" class="form-control" hidden>

                    <div class="col-md-12 mb-4">
                        <input type="text" id="nazivInput" name="naziv" class="form-control"
                               placeholder="Unesite naziv takmicenja" required
                               oninvalid="this.setCustomValidity('<fmt:message key="required.field"/>')"
                               onchange="this.setCustomValidity('')"
                        />
                    </div>

                    <div class="col-md-12 mb-4">
                        <button type="submit" name="takmicenjeSubmit" id="takmicenjeSubmit" class="btn btn-default">
                            Unos
                        </button>
                    </div>
                </form>
            </div>

            <div class="col-md-8 mb-4">
                <div class="table-responsive text-nowrap">
                    <table class="table table-striped" id="takmicenjeTable">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Naziv</th>
                            <th scope="col">Akcija</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="tak" items="${takmicenja}" varStatus="status">
                            <tr id="${tak.id}">
                                <td>${tak.nazivTakmicenja}</td>
                                <td>
                                    <button type="submit" name="update" id="${tak.id}"
                                            class="btn btn-default btn-md takmicenjeUpdate">
                                        <i class="far fa-edit"></i>
                                    </button>
                                    <button type="submit" name="delete" id="${tak.id}"
                                            class="btn btn-danger btn-md takmicenjeDelete">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
</div>
<script>
    $(document).ready(function () {
        var table = $('#takmicenjeTable')

        // DATATABLE INIT
        table.DataTable();

        // INSERT AND UPDATE TABLE
        $('#insertForm').submit(function (e) {
            e.preventDefault();

            takmicenjeJSON = JSON.stringify({
                "id": $('#idInput').val(),
                "nazivTakmicenja": $('#nazivInput').val(),
            });

            $.ajax({
                url: '${pageContext.request.contextPath}/api/takmicenje',
                headers: {'Content-Type': 'application/json'},
                method: 'POST',
                data: takmicenjeJSON,
                success: function (data) {
                    printAlert('<fmt:message key="successful.insert"/>');
                    populateForm("", "");

                    $.ajax({
                        'url': "${pageContext.request.contextPath}/api/takmicenje",
                        'method': "GET",
                        'contentType': 'application/json'
                    }).done(function (data) {
                        table.DataTable().clear();
                        table.DataTable().destroy();
                        table.DataTable({
                            "data": data,
                            "rowId": "id",
                            "columns": [
                                {"data": "nazivTakmicenja"},
                                {
                                    "data": '',
                                    render: function (data, type, row) {
                                        return '<button type="submit" name="update" id="' + row.id + '" class="btn btn-default btn-md takmicenejUpdate"><i class="far fa-edit"></i></button>' +
                                            '<button type="submit" name="delete" id="' + row.id + '"  class="btn btn-danger btn-md takmicenjeDelete"><i class="far fa-trash-alt"></i></button>'
                                    }
                                }
                            ]
                        })
                    })
                },
                error: function (error) {
                    printAlert(error.responseJSON.message);
                }
            });
        });

        // GET FOR UPDATE
        $(document).on('click', '.takmicenjeUpdate', function (e) {
            e.preventDefault();
            $.ajax({
                url: '${pageContext.request.contextPath}/api/takmicenje/' + this.id,
                method: 'GET',
                success: function (data) {
                    populateForm(data.id, data.nazivTakmicenja);
                }
            })
        });

        // DELETE
        $(document).on('click', '.takmicenjeDelete', function (e) {
            e.preventDefault();
            var confirmation = confirm('<fmt:message key="confirmation.delete"/>');
            var deleteId = this.id;
            if (confirmation) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/api/takmicenje/' + deleteId,
                    method: 'DELETE',
                    success: function (data) {
                        printAlert('<fmt:message key='successful.delete'/>');
                        $('#takmicenjeTable #' + deleteId).remove();
                    }
                })
            }
        });

    });

    function populateForm(id, naziv) {
        $('#idInput').val(id);
        $('#nazivInput').val(naziv);
    }
</script>
</body>
</html>
