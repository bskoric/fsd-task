<%--
  Created by IntelliJ IDEA.
  User: branko
  Date: 13.2.21.
  Time: 13:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE>
<html lang="rs">
<head>
    <title>FSD Klub</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<jsp:include page="../../nav/nav.jsp"/>

<div style="width: 90%; margin: auto;">
    <section class="dark-grey-text text-center">

        <%@include file="../../helpers/alert.html" %>

        <h3 class="font-weight-bold pt-4 mb-4">Nastupi</h3>

        <h5 class="font-weight-bold font-italic mb-5">At vero eos et accusamus et iusto odio dignissimos qui cupiditate
            non provident.</h5>

        <div class="row">
            <div class="col-md-4 mb-4">
                <p class="h4 mb-4">Unos</p>

                <form method="post" id="insertForm">
                    <input type="number" id="idInput" name="id" class="form-control" hidden>

                    <div class="col-md-12 mb-4">
                        <select class="browser-default custom-select" id="igracSelect">
                        </select>
                    </div>

                    <div class="col-md-12 mb-4">
                        <select class="browser-default custom-select" id="utakmicaSelect">
                        </select>
                    </div>

                    <div class="col-md-12 mb-4">
                        <input type="number" id="ocenaInput" name="ocena" class="form-control"
                               placeholder="Unesite ocenu igraca" required
                               oninvalid="this.setCustomValidity('<fmt:message key="required.field"/>')"
                               onchange="this.setCustomValidity('')"
                        />
                    </div>

                    <div class="col-md-12 mb-4">
                        <button type="submit" name="ocenaSubmit" id="ocenaSubmit" class="btn btn-default">Unos</button>
                    </div>
                </form>
            </div>

            <div class="col-md-8 mb-4">
                <div class="table-responsive text-nowrap">
                    <table class="table table-striped" id="nastupTable">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Igrac</th>
                            <th scope="col">Klub</th>
                            <th scope="col">Utakmica</th>
                            <th scope="col">Takmicenje</th>
                            <th scope="col">Datum odigravanja</th>
                            <th scope="col">Ocena</th>
                            <th scope="col">Akcija</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="nastup" items="${nastupi}" varStatus="status">
                            <tr id="${nastup.id}">
                                <td>${nastup.igrac.ime} ${nastup.igrac.prezime}</td>
                                <td>${nastup.igrac.klub.naziv}</td>
                                <td>${nastup.utakmica.domacin.naziv} - ${nastup.utakmica.gost.naziv} </td>
                                <td>${nastup.utakmica.takmicenje.nazivTakmicenja}</td>
                                <td>${nastup.utakmica.datumOdigravanja}</td>
                                <td>${nastup.ocenaIgraca}</td>
                                <td>
                                    <button type="submit" name="update" id="${nastup.id}"
                                            class="btn btn-default btn-md nastupUpdate">
                                        <i class="far fa-edit"></i>
                                    </button>
                                    <button type="submit" name="delete" id="${nastup.id}"
                                            class="btn btn-danger btn-md nastupDelete">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
</div>
<script>
    $(document).ready(function () {
        var table = $('#nastupTable')

        // DATATABLE INIT
        table.DataTable();

        // INSERT AND UPDATE TABLE
        $('#insertForm').submit(function (e) {
            e.preventDefault();

            nastupJSON = JSON.stringify({
                "id": $('#idInput').val(),
                "igrac": {
                    "id": $('#igracSelect').val()
                },
                "utakmica": {
                    "id": $('#utakmicaSelect').val()
                },
                "ocenaIgraca": $('#ocenaInput').val()
            });
            console.log(nastupJSON)
            $.ajax({
                url: '${pageContext.request.contextPath}/api/nastup',
                headers: {'Content-Type': 'application/json'},
                method: 'POST',
                data: nastupJSON,
                success: function (data) {
                    printAlert('<fmt:message key="successful.insert"/>');
                    populateForm("");
                    $.ajax({
                        'url': "${pageContext.request.contextPath}/api/nastup",
                        'method': "GET",
                        'contentType': 'application/json'
                    }).done(function (data) {
                        table.DataTable().clear();
                        table.DataTable().destroy();
                        table.DataTable({
                            "data": data,
                            "rowId": "id",
                            "columns": [
                                {
                                    "data": '',
                                    render: function (data, type, row) {
                                        return row.igrac.ime + " " + row.igrac.prezime;
                                    }
                                },
                                {"data": "igrac.klub.naziv"},
                                {
                                    "data": '',
                                    render: function (data, type, row) {
                                        return row.utakmica.domacin.naziv + " - " + row.utakmica.gost.naziv;
                                    }
                                },
                                {"data": "utakmica.takmicenje.nazivTakmicenja"},
                                {"data": "utakmica.datumOdigravanja"},
                                {"data": "ocenaIgraca"},
                                {
                                    "data": '',
                                    render: function (data, type, row) {
                                        return '<button type="submit" name="update" id="' + row.id + '" class="btn btn-default btn-md nastupUpdate"><i class="far fa-edit"></i></button>' +
                                            '<button type="submit" name="delete" id="' + row.id + '"  class="btn btn-danger btn-md nastupDelete"><i class="far fa-trash-alt"></i></button>'
                                    }
                                }
                            ]
                        })
                    })

                },
                error: function (error) {
                    printAlert(error.responseJSON.message);
                }
            });
        });

        // GET MESTO FOR UPDATE
        $(document).on('click', '.nastupUpdate', function (e) {
            e.preventDefault();
            $.ajax({
                url: '${pageContext.request.contextPath}/api/nastup/' + this.id,
                method: 'GET',
                success: function (data) {
                    populateForm(data.ocenaIgraca, data.igrac.id, data.utakmica.id);
                }
            })
        });

        // DELETE MESTO
        $(document).on('click', '.nastupDelete', function (e) {
            e.preventDefault();
            var confirmation = confirm('<fmt:message key="confirmation.delete"/>');
            var deleteId = this.id;
            if (confirmation) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/api/nastup/' + deleteId,
                    method: 'DELETE',
                    success: function (data) {
                        printAlert('<fmt:message key='successful.delete'/>');
                        $('#nastupTable #' + deleteId).remove();
                    }
                })
            }
        });

        $('#igracSelect').select2({
            placeholder: 'Izaberite igraca',
            ajax: {
                url: '${pageContext.request.contextPath}/api/igrac/select',
                dataType: 'json',
                processResults: function (data) {
                    return {
                        results: data.results
                    };
                }
            }
        });

        $('#utakmicaSelect').select2({
            placeholder: 'Izaberite utakmicu',
            ajax: {
                url: '${pageContext.request.contextPath}/api/utakmica/select',
                dataType: 'json',
                processResults: function (data) {
                    return {
                        results: data.results
                    };
                }
            }
        });
    });

    function populateForm(ocena, igracId, utakmicaId) {
        $('#ocenaInput').val(ocena);
        if (igracId && utakmicaId) {
            console.log(igracId)
            $('#igracSelect option[value=' + igracId + ']').attr('selected', 'selected');
            $('#utakmicaSelect').val('1'); // Select the option with a value of '1'
        }
    }
</script>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
</body>
</html>
