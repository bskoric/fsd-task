<%--
  Created by IntelliJ IDEA.
  User: branko
  Date: 13.2.21.
  Time: 13:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE>
<html lang="rs">
<head>
    <title>FSD Klub</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<jsp:include page="../../nav/nav.jsp"/>

<div style="width: 90%; margin: auto;">
    <section class="dark-grey-text text-center">

        <%@include file="../../helpers/alert.html" %>

        <h3 class="font-weight-bold pt-4 mb-4">Nastupi</h3>

        <div class="container mt-5 px-5 pt-5 pb-3 z-depth-1">

            <!--Section: Content-->
            <section class="text-center dark-grey-text">

                <h6 class="font-weight-normal text-uppercase font-small grey-text mb-4"><c:out
                        value="${nastup.igrac.pozicija}"/></h6>
                <!-- Section heading -->
                <h2 class="font-weight-bold"><c:out value="${nastup.igrac.ime}"/> <c:out
                        value="${nastup.igrac.prezime}"/></h2>
                <hr class="w-header my-4">
                <!-- Section description -->
                <p class="text-muted mx-auto mb-5">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                    dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                    officia deserunt mollit id laborum.</p>

                <div class="row text-center d-flex justify-content-center">
                    <div class="col-lg-4 col-md-12 mb-4">
                        <div class="view card-img-100 mx-auto mb-4">
                            <img src="https://mdbootstrap.com/img/illustrations/drawkit-drawing-man-colour.svg"
                                 class="img-fluid" alt="smaple image">
                        </div>
                        <h5 class="font-weight-normal mb-4">Utakmica</h5>
                        <p class="text-muted px-2 mb-lg-0">
                            <c:out value="${nastup.utakmica.domacin.naziv}"/> (<c:out
                                value="${nastup.utakmica.domacin.mesto.naziv}"/>)
                             :
                            <c:out value="${nastup.utakmica.gost.naziv}"/> (<c:out
                                value="${nastup.utakmica.domacin.mesto.naziv}"/>)
                        </p>
                    </div>
                    <div class="col-lg-4 col-md-6 mb-4">
                        <div class="view card-img-100 mx-auto mb-4">
                            <img src="https://mdbootstrap.com/img/illustrations/drawkit-phone-conversation-colour.svg"
                                 class="img-fluid" alt="smaple image">
                        </div>
                        <h5 class="font-weight-normal mb-4">Takmicenje</h5>
                        <p class="text-muted px-2 mb-lg-0">
                            <c:out value="${nastup.utakmica.takmicenje.nazivTakmicenja}"/>
                        </p>
                    </div>
                    <div class="col-lg-4 col-md-6 mb-4">
                        <div class="view card-img-100 mx-auto mb-4">
                            <img src="https://mdbootstrap.com/img/illustrations/app-user-colour.svg" class="img-fluid"
                                 alt="smaple image">
                        </div>
                        <h5 class="font-weight-normal mb-4">Ocena</h5>
                        <p class="text-muted px-2 mb-md-0">
                        <h5><c:out value="${nastup.ocenaIgraca}"/></h5>
                        </p>
                    </div>
                </div>

            </section>
            <!--Section: Content-->


        </div>

        <script>
            $(document).ready(function () {
            });
        </script>

</body>
</html>
