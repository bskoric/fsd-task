<%--
Created by IntelliJ IDEA.
User: branko
Date: 13.2.21.
Time: 11:56
To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>FSD task</title>
    <%@include file="../helpers/imports.html" %>
    <link rel="stylesheet" href="/resources/css/nav.css"/>
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light white scrolling-navbar">
    <div class="container">

        <!-- Brand -->
        <a class="navbar-brand waves-effect" href="/">
            <span style="font-size: 30px; color: darkred">FSD</span>
        </a>

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!-- Left -->
            <ul class="navbar-nav mr-auto">
                <sec:authorize access="hasAnyAuthority('ROLE_ADMIN')">
                    <li class="nav-item">
                        <a class="nav-link waves-effect" href="/igrac">Igraci</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link waves-effect" href="/klub">Klubovi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link waves-effect" href="/mesto">Mesto</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link waves-effect" href="/takmicenje">Takmicenje</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link waves-effect" href="/nastup">Nastup</a>
                    </li>
                </sec:authorize>
            </ul>

            <!-- Right -->
            <ul class="navbar-nav nav-flex-icons">
                <li class="nav-item">
                    <a href="https://www.facebook.com/mdbootstrap" class="nav-link waves-effect" target="_blank">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="https://twitter.com/MDBootstrap" class="nav-link waves-effect" target="_blank">
                        <i class="fab fa-twitter"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="https://github.com/mdbootstrap/bootstrap-material-design" class="nav-link waves-effect"
                       target="_blank">
                        <i class="fab fa-github"></i>
                    </a>
                </li>
            </ul>

        </div>

    </div>
</nav>
<!-- Navbar -->
</body>
</html>
